#!/usr/bin/env bash
# Written by Evils for Hackerspace.Gent,
# based on https://github.com/qwaxys/update-cloudflare

IP=$(something to get this machines public ip)
SETIP=$(dig +short example.com)

# Check to see if the DNSs ip mismatches the current ip and set if so
if [ $SETIP != $IP ]; then

	# Set the IP
	curl -X PUT "https://api.cloudflare.com/client/v4/zones/YOUR_ZONE_ID/dns_records/YOUR_DOMAIN_ID" \
	-H "X-Auth-Email: YOUR_MAIL@example.com" \
	-H "X-Auth-Key: YOUR_AUTH_KEY" \
	--data '{"type":"A","name":"example.org","content":'$IP',"ttl":1,"proxied":false}'

	echo "Update send"

else
	echo "Nothing to update"
fi

